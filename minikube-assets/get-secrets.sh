#!/bin/sh

kubectl get secrets/tiller-secret -n "gitlab-managed-apps" -o "jsonpath={.data['ca\.crt']}" | base64 --decode > ~/.helm/tiller-ca.crt
kubectl get secrets/tiller-secret -n "gitlab-managed-apps" -o "jsonpath={.data['tls\.crt']}" | base64 --decode > ~/.helm/tiller.crt
kubectl get secrets/tiller-secret -n "gitlab-managed-apps" -o "jsonpath={.data['tls\.key']}" | base64 --decode > ~/.helm/tiller.key

echo secrets are in ~/.helm/tiller-ca.crt, ~/.helm/tiller.crt and ~/.helm/tiller.key
