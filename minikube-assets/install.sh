kubectl get pvc -n gitlab-managed-apps | grep '^data-elastic-stack-elasticsearch-data' > /dev/null

if [ "$?" -eq "1" ]; then
    helm install stable/elastic-stack  --name elastic-stack --tiller-connection-timeout 30 --tls  --tls-ca-cert ~/.helm/tiller-ca.crt --tls-cert ~/.helm/tiller.crt --tls-key ~/.helm/tiller.key --tiller-namespace gitlab-managed-apps --namespace gitlab-managed-apps --values efk-values.yaml --set on_minikube=true
else
    echo "Delete those pvcs with delete-pvc.sh"
fi
